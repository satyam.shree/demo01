package com.example.cricketplayers.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cricketplayers.adapter.RecyclerViewAdapter
import com.example.cricketplayers.databinding.FragmentHomeBinding
import com.example.cricketplayers.db.UserDataBase
import com.example.cricketplayers.db.userData1
import com.example.cricketplayers.repository.UserRepository
import com.example.cricketplayers.viewModel.AddUserViewModel
import com.example.cricketplayers.viewModel.MainViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.view.*

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    private lateinit var homeBinding: FragmentHomeBinding
    private lateinit var mainViewModel: AddUserViewModel
    private lateinit var userRepository: UserRepository
    private lateinit var adapter:RecyclerViewAdapter


    @SuppressLint("NotifyDataChanged")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {


      homeBinding= FragmentHomeBinding.inflate(inflater)
        userRepository = UserRepository(UserDataBase.getDatabase(requireActivity().applicationContext))
        mainViewModel=ViewModelProvider(this,MainViewModelFactory(userRepository)).get(AddUserViewModel::class.java)

        mainViewModel.list.observe(requireActivity(), Observer {
                it ->
            val list = ArrayList<userData1>()

            it.forEach {

                list.add(it)
            }
            if (list.size > 0) {
                if (isAdded) {
                    homeBinding.recyclerview.layoutManager=LinearLayoutManager(requireContext())
                    adapter = RecyclerViewAdapter(list,mainViewModel, requireContext())
                    homeBinding.recyclerview.adapter = adapter
                    homeBinding.dataNotFoundHome.visibility = View.INVISIBLE
                }
            }
            else{
                homeBinding.dataNotFoundHome.visibility = View.VISIBLE
                homeBinding.recyclerview.adapter = null
            }


        })



        homeBinding.serachView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val size=newText?.length
                if (size != null) {
                    if (size>3){

                        mainViewModel.list.observe(requireActivity()) {
                            val list = ArrayList<userData1>()

                            it.forEach {
                                if (it.name.contains(newText,true)) {
                                    list.add(it)
//                                    if (list.size > 0) {
                                    if (isAdded) {
                                        adapter = RecyclerViewAdapter(list,mainViewModel, requireContext())
                                        homeBinding.recyclerview.adapter = adapter
//                                            binding.Placeholder.visibility = View.INVISIBLE
//                                        }
                                    }
                                }
                            }
                        }
                    }
                    Log.d("msg","afterText $size")
                }
                mainViewModel.list.observe(requireActivity(), Observer {

                    homeBinding.recyclerview.layoutManager=LinearLayoutManager(requireContext())
                    val adapter = RecyclerViewAdapter(it,mainViewModel,requireContext())
//                    adapter.notifyDataSetChanged()

                    homeBinding.recyclerview.adapter=adapter
                })
                return true
            }
        })
        return homeBinding.root
    }


    override fun onResume() {
        super.onResume()
        requireActivity().app_bar_main.btn.visibility=View.VISIBLE
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}