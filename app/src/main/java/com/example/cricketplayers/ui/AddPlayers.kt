package com.example.cricketplayers.ui

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.cricketplayers.R
import com.example.cricketplayers.databinding.FragmentAddPlayersBinding
import com.example.cricketplayers.db.UserDataBase
import com.example.cricketplayers.db.userData1
import com.example.cricketplayers.repository.UserRepository
import com.example.cricketplayers.viewModel.AddUserViewModel
import com.example.cricketplayers.viewModel.MainViewModelFactory
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.app_bar_main.view.*
import kotlinx.android.synthetic.main.fragment_add_players.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import kotlinx.android.synthetic.main.playerslist.*
import java.util.*


class AddPlayers : Fragment() {

    private lateinit var mainViewModel: AddUserViewModel
    private lateinit var addBinding: FragmentAddPlayersBinding
    private lateinit var  userRepository: UserRepository
    private lateinit var toooo : CharSequence
    private val cameraRequest = 1888
    private val galleryrequest = 18
    private val pickImage = 100
    private var selectedimg:Bitmap?=null
//    private var selectimage:Bitmap?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        if (ContextCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.CAMERA
            )
            == PackageManager.PERMISSION_DENIED
        )
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(android.Manifest.permission.CAMERA),
                cameraRequest
            )

        if (ContextCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            )
            == PackageManager.PERMISSION_DENIED
        )
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                galleryrequest
            )

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        addBinding= FragmentAddPlayersBinding.inflate(inflater)
        userRepository= UserRepository(UserDataBase.getDatabase(requireActivity().applicationContext))
        mainViewModel=ViewModelProvider(this, MainViewModelFactory(userRepository)).get(AddUserViewModel::class.java)


//        addBinding= FragmentAddPlayersBinding.inflate(inflater)

        mainViewModel.list.observe(requireActivity(), Observer {

        })

        val myCalendar=Calendar.getInstance()

        val dataPicker = DatePickerDialog.OnDateSetListener{dataPicker,year,month,dayofMonth ->
            myCalendar.set(Calendar.YEAR,year)
            myCalendar.set(Calendar.MONTH,month)
            myCalendar.set(Calendar.DAY_OF_MONTH,dayofMonth)
            updateLable(myCalendar)
        }


        addBinding.btnSavedetails.setOnClickListener {

            addBinding.apply {

                if (etNames.text.isEmpty()){
                    etNames.error="mandatory"
                    return@setOnClickListener
                }
                else if(etTeam.text.isEmpty()){
                    etTeam.error="mandatory"
                    return@setOnClickListener
                }
                else if(etCountry.text.isEmpty() ){
                    etCountry.error="mandatory"
//                    Toast.makeText(requireContext(),"Enter DOB", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                else if(etDob.text.isEmpty() ){
                    Toast.makeText(requireContext(),"Enter DOB", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                else if(!radioButton.isChecked && !radioButton2.isChecked ){
                    Toast.makeText(requireContext(),"Select Batsman or Bowler", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }

                else if(etMatches.text.isEmpty()){
                    etMatches.error="mandatory"
                    return@setOnClickListener
                }


                else if(etRuns.text.isEmpty()){
                    etRuns.error="mandatory"
                    return@setOnClickListener
                }


                else if(etWickets.text.isEmpty()){
                    etWickets.error="mandatory"
                    return@setOnClickListener
                }
                if (selectedimg==null){

                    Toast.makeText(requireContext(),"Select profile", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
            }



            addUserDetail()
            et_names.text.clear()
            et_team.text.clear()
            et_country.text.clear()
            et_matches.text.clear()
            et_runs.text.clear()
            et_wickets.text.clear()
            findNavController().navigate(R.id.add_to_nav_home)
//            findNavController().navigate(R.id.nav_home)
        }

        addBinding.etDob.setOnClickListener{
            val c: java.util.Calendar = java.util.Calendar.getInstance()
            val mYear: Int = c.get(java.util.Calendar.YEAR) // current year
            val mMonth: Int = c.get(java.util.Calendar.MONTH) // current month
            val mDay: Int = c.get(java.util.Calendar.DAY_OF_MONTH) // current day

            // date picker dialog

            val datePickerDialog = DatePickerDialog(
                requireActivity(),
                { view, year, month, day -> // set day of month , month and year value in the edit text
                    if (c.get(java.util.Calendar.YEAR) - year >= 18) {
                        addBinding.etDob.text =
                            day.toString() + "/" + (month + 1) + "/" + year

                    } else {
                        Toast.makeText(
                            requireActivity(),
                            "Under 18 Player Not Allowed",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }, mYear, mMonth, mDay

            )
            datePickerDialog.show()
        }

        addBinding.ibCalender.setOnClickListener{
            DatePickerDialog(requireActivity(),dataPicker,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }

//            addBinding.radiogroup.setOnCheckedChangeListener { radioGroup, i ->
//            if(i== R.id.radioButton)
//                Toast.makeText(context,radioButton.text,Toast.LENGTH_LONG).show()
//            if(i==R.id.radioButton2)
//                Toast.makeText(context,radioButton2.text,Toast.LENGTH_LONG).show()
//        }

        return addBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iv_playerdp.setOnClickListener {



           /* val toolbar: Toolbar = findViewById(R.id.toolbar) as Toolbar

            setSupportActionBar(toolbar)
            toolbar.findViewById(R.id.toolbar_title).setText(toolbar.getTitle())

            getSupportActionBar().setDisplayShowTitleEnabled(false)*/

            val builder= AlertDialog.Builder(requireContext())
            builder.setTitle("select Image")
            builder.setMessage("choose your Option?")
            builder.setPositiveButton("Gallery")
            {
                    dailog,which->
                dailog.dismiss()

                val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                startActivityForResult(gallery, pickImage)

            }
            builder.setNegativeButton("camera"){dailog,which->
                dailog.dismiss()

                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, cameraRequest)
            }
            val dailog:AlertDialog=builder.create()
            dailog.show()
        }
    }


    private fun addUserDetail() {
        mainViewModel.addUserData(
            userData1(null ,
                selectedimg!!,
                addBinding.etNames.text.trim().toString(),
                addBinding.etTeam.text.trim().toString(),
                addBinding.etCountry.text.trim().toString(),
                addBinding.etMatches.text.trim().toString(),
                addBinding.etRuns.text.trim().toString(),
                addBinding.etWickets.text.trim().toString(),
                addBinding.radioButton.isChecked,
                addBinding.radioButton2.isChecked,
                addBinding.etDob.text.trim().toString(),
                false


            )
        )
    }

    private fun updateLable(myCalendar: Calendar){
        val myFormat ="dd-MM-yyyy"
        val sdf=java.text.SimpleDateFormat(myFormat,Locale.UK)
        et_dob.setText(sdf.format(myCalendar.time))
    }

//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//        requireActivity().toolbar.btn.visibility = View.INVISIBLE
//    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == cameraRequest) {

            iv_playerdp.setImageBitmap(data?.extras?.get("data") as Bitmap)
            selectedimg=data.extras?.get("data") as Bitmap
        }


        if (resultCode == AppCompatActivity.RESULT_OK && requestCode == pickImage) {

            val bitmap=MediaStore.Images.Media.getBitmap(context?.contentResolver,data?.data)
            iv_playerdp.setImageBitmap(bitmap)
            selectedimg=bitmap

        }
    }

}