package com.example.cricketplayers.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cricketplayers.R
import com.example.cricketplayers.databinding.FragmentInformatonBinding
import com.example.cricketplayers.db.userData1


class Informaton : Fragment() {

    private lateinit var binding: FragmentInformatonBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragmentInformatonBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data=requireActivity().intent.getParcelableExtra<userData1>("key")
        binding.name.text=data?.name
        binding.tvCountry.text=data?.country
        binding.etBorn.text=data?.dob
        binding.cirImg.setImageBitmap(data?.image)
//        Log.d("msg","${data?.dob}")
        when(data?.typebatsman){
            true->binding.etRole.text="Batsman"
            else->binding.etRole.text="Bowler"
        }



    }


}