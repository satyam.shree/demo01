package com.example.cricketplayers.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cricketplayers.R
import com.example.cricketplayers.databinding.FragmentBowlingBinding
import com.example.cricketplayers.db.userData1

class Bowling : Fragment() {
    private lateinit var binding: FragmentBowlingBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragmentBowlingBinding.inflate(inflater)
        return binding.root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val data=requireActivity().intent.getParcelableExtra<userData1>("key")
        binding.matchesBowling.text=data?.matches
        binding.testwick.text=data?.wickets

    }

}