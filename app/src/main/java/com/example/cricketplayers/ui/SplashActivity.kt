package com.example.cricketplayers.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.cricketplayers.MainActivity
import com.example.cricketplayers.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash2)

        supportActionBar?.hide()

        CoroutineScope(Dispatchers.Main).launch {
            val intent= Intent(this@SplashActivity, MainActivity::class.java)
            delay(3000)
            startActivity(intent)
            finish()

        }
    }
}