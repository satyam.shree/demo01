package com.example.cricketplayers.ui.myfav

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cricketplayers.R
import com.example.cricketplayers.adapter.AdapterBatsman
import com.example.cricketplayers.databinding.FragmentMyFavouriteBinding
import com.example.cricketplayers.db.UserDataBase
import com.example.cricketplayers.db.userData1
import com.example.cricketplayers.repository.UserRepository
import com.example.cricketplayers.viewModel.AddUserViewModel
import com.example.cricketplayers.viewModel.MainViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.view.*


class MyFavourite : Fragment() {
    private lateinit var binding: FragmentMyFavouriteBinding
    private lateinit var viewModelBatsman: AddUserViewModel
    private lateinit var repBatsman: UserRepository
    private lateinit var adapter: AdapterBatsman


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragmentMyFavouriteBinding.inflate(inflater)
        return binding.root



    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().app_bar_main.btn.visibility=View.INVISIBLE

        repBatsman = UserRepository(UserDataBase.getDatabase(requireActivity().applicationContext))
        viewModelBatsman= ViewModelProvider(this, MainViewModelFactory(repBatsman)).get(AddUserViewModel::class.java)

        viewModelBatsman.list.observe(requireActivity(), Observer {

            binding.rvRecyleFavourite.layoutManager = LinearLayoutManager(requireContext())

            val list=ArrayList<userData1>()
            it.forEach {
                if (it.favourite == true){
                    list.add(it)
                }
            }
            if (list.size>0){
                if (isAdded){
                    adapter = AdapterBatsman(list, requireContext())
                    binding.rvRecyleFavourite.adapter = adapter
                    binding.favNotFound.visibility= View.INVISIBLE
                }
            } else {
                Log.d("tag", "no data found")
                binding.favNotFound.visibility= View.VISIBLE
            }


//            adapter.notifyDataSetChanged()

//            binding.rvRecyleFavourite.adapter = adapter

        })

    }
}