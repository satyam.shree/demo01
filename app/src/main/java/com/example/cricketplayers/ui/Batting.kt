package com.example.cricketplayers.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cricketplayers.R
import com.example.cricketplayers.databinding.FragmentBattingBinding
import com.example.cricketplayers.db.userData1
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.app_bar_main.view.*

class Batting : Fragment() {
    private lateinit var binding: FragmentBattingBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentBattingBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val data=requireActivity().intent.getParcelableExtra<userData1>("key")
        binding.runss.text=data?.runs
        binding.matchesBatting.text=data?.matches

    }


}