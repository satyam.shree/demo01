package com.example.cricketplayers.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.cricketplayers.MainActivity
import com.example.cricketplayers.R
import com.example.cricketplayers.adapter.PageAdapter
import com.example.cricketplayers.db.UserDataBase
import com.example.cricketplayers.db.userData1
import com.example.cricketplayers.repository.UserRepository
import com.example.cricketplayers.viewModel.AddUserViewModel
import com.example.cricketplayers.viewModel.MainViewModelFactory
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {

    private lateinit var viewModelFav: AddUserViewModel
    private lateinit var repFavourite: UserRepository
    private  var flag:Boolean=false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        supportActionBar?.hide()

        repFavourite = UserRepository(UserDataBase.getDatabase(applicationContext))
        viewModelFav= ViewModelProvider(this, MainViewModelFactory(repFavourite)).get(AddUserViewModel::class.java)


        val viewPagerAdapter= PageAdapter(supportFragmentManager)

        viewPagerAdapter.addFragment(Informaton())
        viewPagerAdapter.addFragment(Batting())
        viewPagerAdapter.addFragment(Bowling())
        view_pager.adapter = viewPagerAdapter


        tab_layout.layoutDirection = View.LAYOUT_DIRECTION_LTR



        tab_layout.setupWithViewPager(view_pager)
        view_pager.currentItem = 0
        tab_layout.getTabAt(0)?.text="Info"
        tab_layout.getTabAt(1)?.text="Batting"
        tab_layout.getTabAt(2)?.text="Bowling"

        val data=intent.getParcelableExtra<userData1>("key")

        title_bar.text=data?.name

        viewModelFav.list.observe(this, Observer {

            flag=it[data?.id!!-1].favourite
            if (flag){
                btn_favourite.visibility=View.VISIBLE
            }
            else{
                btn_favouriteFilled.visibility=View.VISIBLE
            }
            Log.d("msg","${flag}")
        })

        btn_back.setOnClickListener{
            val intent= Intent(this, MainActivity::class.java)
            startActivity(intent)

        }



        btn_favourite.setOnClickListener{

            flag=!flag

            if (flag){
                btn_favouriteFilled.visibility=View.VISIBLE
                if (data != null) {
                    viewModelFav.update(userData1(
                        data.id,
                        data.image,
                        data.name,
                        data.team,
                        data.country,
                        data.matches,
                        data.runs,
                        data.wickets,
                        data.typebatsman,
                        data.typebowler,
                        data.dob,
                        true
                    ))

                }
                Toast.makeText(this,"fav pressed", Toast.LENGTH_SHORT).show()}
            else{
                btn_favourite.visibility=View.INVISIBLE
                if (data != null) {
                    viewModelFav.update(
                        userData1(
                        data.id,
                        data.image,
                        data.name,
                        data.team,
                        data.country,
                        data.matches,
                        data.runs,
                        data.wickets,
                            data.typebatsman,
                            data.typebowler,
                            data.dob,
                        false
                    )
                    )
                }
                Toast.makeText(this,"fav unpressed",Toast.LENGTH_SHORT).show()
            }
        }
    }
}