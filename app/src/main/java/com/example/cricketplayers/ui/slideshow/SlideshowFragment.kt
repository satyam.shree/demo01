package com.example.cricketplayers.ui.slideshow

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cricketplayers.adapter.AdapterBatsman
import com.example.cricketplayers.databinding.FragmentSlideshowBinding
import com.example.cricketplayers.db.UserDataBase
import com.example.cricketplayers.db.userData1
import com.example.cricketplayers.repository.UserRepository
import com.example.cricketplayers.viewModel.AddUserViewModel
import com.example.cricketplayers.viewModel.MainViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.view.*

class SlideshowFragment : Fragment() {
    private lateinit var binding:FragmentSlideshowBinding
    private lateinit var viewModelBatsman: AddUserViewModel
    private lateinit var repBatsman: UserRepository
    private lateinit var adapter: AdapterBatsman

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragmentSlideshowBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().app_bar_main.btn.visibility=View.INVISIBLE

        repBatsman = UserRepository(UserDataBase.getDatabase(requireActivity().applicationContext))
        viewModelBatsman=ViewModelProvider(this, MainViewModelFactory(repBatsman)).get(AddUserViewModel::class.java)

        viewModelBatsman.list.observe(requireActivity(), Observer {

            binding.rvRecyleBowler.layoutManager = LinearLayoutManager(requireContext())

            val list=ArrayList<userData1>()
            it.forEach {
                if (it.typebowler == true){
                    list.add(it)
                }
            }
            if (list.size>0){
                if (isAdded){
                    adapter = AdapterBatsman(list, requireContext())
                    binding.rvRecyleBowler.adapter = adapter
                    binding.dataNotFoundBowler.visibility= View.INVISIBLE
                }
            } else {
                Log.d("tag", "no data found")
                binding.dataNotFoundBowler.visibility= View.VISIBLE
            }

//            adapter.notifyDataSetChanged()

//            binding.rvRecyleBowler.adapter = adapter

        })
    }
}