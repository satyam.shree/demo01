package com.example.cricketplayers.viewModel

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.cricketplayers.db.userData1
import com.example.cricketplayers.repository.UserRepository

class AddUserViewModel(private val userRepository: UserRepository): ViewModel(){

    val list : LiveData<List<userData1>>
        get() {
            return userRepository.users
        }

    fun addUserData(userData1: userData1)
    {
        userRepository.add(userData1)
    }

    fun update(userData1:userData1){
        userRepository.updateData(userData1)
    }
}