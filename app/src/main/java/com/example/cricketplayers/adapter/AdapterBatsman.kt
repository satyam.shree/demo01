package com.example.cricketplayers.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cricketplayers.R
import com.example.cricketplayers.databinding.PlayerslistBinding
import com.example.cricketplayers.db.userData1
import com.example.cricketplayers.ui.DetailsActivity
import kotlinx.android.synthetic.main.playerslist.view.*


class AdapterBatsman(private val userList:List<userData1>,private val context: Context):
    RecyclerView.Adapter<AdapterBatsman.ViewHolderClass>() {
    private val binding:PlayerslistBinding?=null
    inner class ViewHolderClass(view: View?): RecyclerView.ViewHolder(view!!){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderClass {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.playerslist,parent,false)
        return ViewHolderClass(view)




    }




    override fun onBindViewHolder(holder: ViewHolderClass, position: Int) {
//        Log.d("msg","adapter : ${userList[position].batsman} + pos : $position")
        holder.itemView.apply {

            tv_countryname.text = userList[position].country
            img_listprofile.setImageBitmap(userList[position].image)
            tv_playername.text = userList[position].name
        }
        holder.itemView.cardview.setOnClickListener{
            val intent= Intent(context, DetailsActivity::class.java)
            intent.putExtra("key",userList[position])
            context.startActivity(intent)
        }
    }
    override fun getItemCount(): Int {
        return userList.size
    }
}