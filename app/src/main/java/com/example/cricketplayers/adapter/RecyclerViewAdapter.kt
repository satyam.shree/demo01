package com.example.cricketplayers.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cricketplayers.R
import com.example.cricketplayers.databinding.FragmentHomeBinding
import com.example.cricketplayers.databinding.PlayerslistBinding
import com.example.cricketplayers.db.userData1
import com.example.cricketplayers.ui.DetailsActivity
import com.example.cricketplayers.viewModel.AddUserViewModel
import kotlinx.android.synthetic.main.nav_header_main.view.*
import kotlinx.android.synthetic.main.playerslist.view.*

class RecyclerViewAdapter(private val userList:List<userData1>,private val addUserViewModel: AddUserViewModel,private val context: Context):RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>()
{
    private var binding:PlayerslistBinding?=null
//    private var playerList= emptyList<userData1>()

    inner class  ViewHolder(val binding: PlayerslistBinding):RecyclerView.ViewHolder(binding.root){

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
          binding= PlayerslistBinding.inflate(LayoutInflater.from(parent.context),parent,false)
          return ViewHolder(binding!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data=userList[position]
        holder.binding.imgListprofile.setImageBitmap(data.image)
        holder.binding.tvPlayername.text=data.name
        holder.binding.tvCountryname.text=data.country

        holder.itemView.cardview.setOnClickListener{
            val intent= Intent(context, DetailsActivity::class.java)
            intent.putExtra("key",data)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }
}