package com.example.cricketplayers.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface UsersDao {

    @Insert
    fun insertUserDetail(userData1: userData1)

    @Query("select * from userData1")
    fun getAllUser() : LiveData<List<userData1>>

    @Update
    fun update(userData1: userData1)

}