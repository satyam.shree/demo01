package com.example.cricketplayers.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [userData1::class], version = 3)
abstract class UserDataBase : RoomDatabase() {

    abstract fun userDao():UsersDao
    companion object{
        @Volatile
        private var INSTANCE: UserDataBase?=null
        fun getDatabase(context: Context):UserDataBase{
            if (INSTANCE == null) {
                synchronized(this) {}
                INSTANCE = Room.databaseBuilder(
                    context,
                    UserDataBase::class.java, "userDB"
                ).fallbackToDestructiveMigration().build()
            }
            return INSTANCE!!
        }
    }
}