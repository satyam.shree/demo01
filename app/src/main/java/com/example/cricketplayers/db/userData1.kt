package com.example.cricketplayers.db

import android.graphics.Bitmap
import android.os.Parcelable
import android.security.identity.AccessControlProfile
import androidx.room.*
import com.example.cricketplayers.Converter
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "userData1")
@TypeConverters(Converter::class)
@Parcelize
data class userData1(
    @PrimaryKey(autoGenerate = true)
    val id:Int? = null,
    @ColumnInfo(name = "img")val image:Bitmap,
    @ColumnInfo(name = "name")val name:String,
    @ColumnInfo(name = "team")val team:String,
    @ColumnInfo(name = "country")val country:String,
    @ColumnInfo(name = "matches")val matches:String,
    @ColumnInfo(name = "runs")val runs:String,
    @ColumnInfo(name = "wickets")val wickets:String,
    @ColumnInfo(name = "batsman")val typebatsman:Boolean?=null,
    @ColumnInfo(name = "bowler")val typebowler: Boolean?=null,
    @ColumnInfo(name = "dob")val dob:String,
    @ColumnInfo(name = "favourite") val favourite:Boolean
    ): Parcelable

//@PrimaryKey(autoGenerate = true)
//val id:Int? = null,
//@ColumnInfo(name = "img") val img:ByteArray?=null,
//@ColumnInfo(name = "name") val name:String,
//@ColumnInfo(name = "team") val team:String,
//@ColumnInfo(name = "country") val country:String,
//@ColumnInfo(name = "matches") val matches:String,
//@ColumnInfo(name = "runs") val runs:String,
//@ColumnInfo(name = "wickets") val wickets:String,
//@ColumnInfo(name = "dob") val dob:String,
//@ColumnInfo(name = "batsman") val batsman:Boolean,
//@ColumnInfo(name = "bowler") val Bowler:Boolean,
//@ColumnInfo(name = "favourite") val favourite:Boolean