package com.example.cricketplayers.repository

import androidx.lifecycle.LiveData
import com.example.cricketplayers.db.UserDataBase
import com.example.cricketplayers.db.userData1
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserRepository(private val userDataBase: UserDataBase) {


    val users : LiveData<List<userData1>>
        get() = userDataBase.userDao().getAllUser()

    fun add(userData1: userData1)
    {
        CoroutineScope(Dispatchers.IO).launch {
            userDataBase.userDao().insertUserDetail(userData1 )
        }
    }

    fun updateData(userData1: userData1){
        CoroutineScope(Dispatchers.IO).launch {
            userDataBase.userDao().update(userData1)
        }

    }
}